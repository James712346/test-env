ARG GOLANG_VERSION=1.20
FROM golang:${GOLANG_VERSION}-bullseye

ARG GIT_VERSION=2.40.0
ARG GOLANGCI_LINT_VERSION=1.52.2

ARG GITEA_ID=1000
ARG GITEA_GID=1000

ENV GIT_VERSION=${GIT_VERSION}
ENV GOLANGCI_LINT_VERSION=${GOLANGCI_LINT_VERSION}

RUN addgroup \
    --gid $GITEA_GID \
    gitea && \
  adduser \
    --gecos '' \
    --shell /bin/bash \
    --uid $GITEA_ID \
    --gid $GITEA_GID \
    gitea

# version-lock git to v${GIT_VERSION} to match alpine 3.17
RUN curl -SL https://github.com/git/git/archive/v${GIT_VERSION}.tar.gz \
    | tar -xz -C /go \
    && apt-get update \
    && apt-get install -y libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev \
    && make -C /go/git-${GIT_VERSION} prefix=/usr all \
    && make -C /go/git-${GIT_VERSION} prefix=/usr install \
    && rm -rf /go/git-${GIT_VERSION} \
# install git-lfs
    && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
    && apt-get install -y git-lfs \
# install golangci-lint
    && go install github.com/golangci/golangci-lint/cmd/golangci-lint@v${GOLANGCI_LINT_VERSION} \
    && golangci-lint --version \
    && curl -sL https://deb.nodesource.com/setup_18.x | bash - && apt-get -qqy install nodejs

USER gitea
RUN git config --global --add safe.directory '*'
USER root
RUN git config --global --add safe.directory '*'
